# Package

version       = "0.1.0"
author        = "Anubhav (eagledot)"
description   = "Reading and Writing utilities for .wav files."
license       = "MIT"
srcDir        = "src"


# Dependencies
requires "nim >= 1.0.0"

skipDirs = @["examples"]
