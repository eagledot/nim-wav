# Utilities for Reading and Writing ``.wav`` files 
_Note: Code is pretty rough right now and currently supports  ``.wav`` files  with ``INT16 PCM encoding`` only._

## Installation:
```$ nimble install https://gitlab.com/eagledot/nim-wav```


## Usage:
```nim
import wav
```

For more complete usage see examples in ``./examples/`` directory.