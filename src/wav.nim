type
  wavHeaderobj* = object
    chunk_id*: array[4,char]
    chunk_size: uint32 
    format: array[4,char]
    fmtchunk_id: array[4,char]
    fmtchunk_size: uint32
    audio_format: uint16
    num_channels*: uint16
    sample_rate*: uint32
    byte_rate: uint32
    block_align: uint16
    bps: uint16
    datachunk_id: array[4,char]
    datachunk_size*: uint32
    
    #new type which is a reference to an object just declared above.
  wavHeaderRef* = ref wavHeaderobj



#new type for .wav files.
type 
    wavObj* = object
        sampleRate: uint32
        nChannels: uint16
        pcmData: seq[int16]
        fileName: string  #will be used to later at time of closing the file..to put all the data at once..do not do it for large files.
    wavRef* = ptr wavObj


#based on these two flags we would open a .wav file..

proc wavRead*(fileName: string): tuple[fd: File,sampleRate: int,nChannels: int] = 
   
    var fd =  open(fileName,fmRead)
    var header =  new(wavHeaderobj)
    #populate the header.
    
    #read all the header data of wav file into the header.
    var count = fd.readBuffer(addr(header.chunk_id),sizeof(wavHeaderobj))
    assert count == sizeof(wavHeaderobj)
    #make sure format is PCM Int16 ..others are not allowed for now.
    assert header.audio_format == uint16(1)
    echo("dataChunksize: ",header.datachunk_size)
    return (fd,int(header.sample_rate),int(header.num_channels))

proc wavWrite*(fileName: string,sampleRate:uint32 = 16000'u32,nChannels:uint16 = 1'u16): wavObj =
    
    result.pcmData = @[0'i16]
    result.fileName = fileName
    result.nChannels = nChannels
    result.sampleRate = sampleRate
    return result

       


proc readChunk*(f: File, n_samples: Natural,n_channels: int ): seq[int16] =
    #n_samples is the number of samples not bytes.
    result = newSeq[int16](n_samples*n_channels)
    var bytes_read = f.readBuffer(addr(result[0]),n_channels*n_samples*sizeof(int16))

    #I donot know if slice exists for sequences.
    if bytes_read < n_channels*n_samples*sizeof(int16):
        var temp = newSeq[int16](int(bytes_read/2))
        for i  in 0..int(bytes_read/2)-1:
            temp[i] = result[i]
        #echo("I am done now!!")
        return temp
    return result

proc writeChunk*(fd: var wavObj,data : seq[int16]) =
    #Store the data.
    fd.pcmData = fd.pcmData & data
proc writeChunk*(fd: wavRef,data: seq[int16]) =
    fd.pcmData = fd.pcmData & data


#overloading the close function.
proc close*(fd: wavObj) =
    #write all the pcmData collected upto now to the disk...along with header
    var f = open(fd.fileName,fmWrite)

    discard f.writeBytes(cast[array[4,uint8]](['R','I','F','F']),0,4)  #[R I F F]#
    #Will be updated once we are done writing data..have to update this value every time we write a chunk of data.
    discard f.writeBytes(cast[array[sizeof(uint32),uint8]](36'u32 + uint32(len(fd.pcmData))),0,sizeof(uint32))
    discard f.writeBytes(cast[array[4,uint8]](['W','A','V','E']),0,4)   #[W A V E]#
    discard f.writeBytes(cast[array[4,uint8]](['f','m','t',' ']),0,4)   #[fmt ]#
    discard f.writeBytes(cast[array[sizeof(uint32),uint8]](16'u32),0,sizeof(uint32))
    
    #write audio_format 1 for PCM data.
    discard f.writeBytes(cast[array[sizeof(uint16),uint8]](1'u16),0,sizeof(uint16))
    #write num_channels
    discard f.writeBytes(cast[array[sizeof(uint16),uint8]](fd.nChannels),0,sizeof(uint16))
    #write Samplerate
    discard f.writeBytes(cast[array[sizeof(uint32),uint8]](fd.sampleRate),0,sizeof(uint32))
    #write bytes per second ... 2 is here for int16.
    discard f.writeBytes(cast[array[sizeof(uint32),uint8]](cast[uint32](fd.sampleRate*fd.nChannels*2)),0,sizeof(uint32))
    discard f.writeBytes(cast[array[sizeof(uint16),uint8]](2'u16),0,sizeof(uint16))
    discard f.writeBytes(cast[array[sizeof(uint16),uint8]](16'u16),0,sizeof(uint16))
    discard f.writeBytes(cast[array[4,uint8]](['d','a','t','a']),0,4)
    #AT beginning would be set to zero..keep updating..when writeChunks of data.
    discard f.writeBytes(cast[array[sizeof(uint32),uint8]](uint32((len(fd.pcmData)-1)*sizeof(int16))),0,4)
    
    if len(fd.pcmData)>1:
        
        discard f.writeBuffer(unsafeAddr(fd.pcmData[1]),(len(fd.pcmData)-1)*sizeof(int16))

    f.close()



    