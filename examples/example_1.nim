
import wav
#open a file to be read...
var (f1,sampleRate,channels) = wavRead("test1.wav")
echo("SampleRate: ",sampleRate)
echo("Channels:  ",channels)
#read some data..
var data = f1.readChunk(16000*9,channels) #use len(data) to know the end of .wav data.
#echo(len(data))
f1.close()
    


#open a wav file to be written....provide the sampleRate and numChannels while opening a wavObj in write mode. 
var f = wavWrite("test2.wav",uint32(sampleRate),uint16(channels))
#var f2: wavRef = addr(f)
#write chunk of data to f2
#f2.writeChunk(data)
f.writeChunk(data)
#f2.writeChunk(data)
#close the file.
f.close()


echo("All done")